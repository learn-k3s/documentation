## Kube c'est dur! ... ou pas?

A chaque fois que j'ai essayé de me mettre à Kubernetes, j'ai rapidement abandonné car je n'arrivais pas à mettre en oeuvre mes workflows de développement habituels. J'ai souvent trouvé la documentation indigeste (est-ce que c'est parce que ça ne s'adresse pas à des développeurs? ... Ou que je suis vraiment mauvais?).

Je ne crois pas être le seul dans ce cas là. On entend souvent dire par des bien pensants de l'IT que **"Kubernetes c'est de la 💩 en 🥫"** et une solution pratique est de se réfugier derrière ce courant de pensées non argumentées mais bien pratique (ce que j'ai fait un moment).

Cependant (à travers mon métier, mes clients, des conférences, ...) j'ai rencontré (et rencontre encore) énormément de gens qui ont l'air d'apprécier énormément cette "chose en boîte", et qui ont même des projets qui fonctionnent bien. Il était donc temps de m'y remettre et de ne pas lâcher l'affaire.

Après une consommation excessive de livres (jamais terminés), de présentations dans des conférences, de tutos qui vous expliquent comment "monter" un cluster Kube mais c'est tout, ou d'autres comment déployer une 1ère fois votre application mais jamais comment le faire une deuxième fois, quelques trainings vidéos (dont certains plutôt pas mal), je suis arrivé aux conclusion suivantes:

- Le principe de base d'orchestrateur ultime (une application qui fait tout) de containers avec la promesse de gommer le côté "vendor lockin" des Amazon, Google, Azure, ... c'est plutôt pas mal du tout (Horacio Gonzalez [@lostinbrittany](witter.com/lostinbrittany) a fait un très bon talk sur le sujet [Multicloud ou comment ne pas mettre tous les oeufs dans le même cloud](https://noti.st/lostinbrittany/DT9RH8))
- Kubernetes (managé ou pas) c'est bien d'avoir une équipe qui sait s'en servir
- **Ce n'est pas aux développeurs de gérer le cluster Kube** (précision du point précédent)

Par contre en tant que développeur, j'aime bien comprendre le minimume vital pour avoir la garantie que mes applications vont se déployer correctement sur la plateforme cible (Kube donc). Donc sans pour autant devenir un spécialiste, je voudrais pouvoir être capable:

- D'installer un "petit cluster" sur ma machine (et de savoir reproduire l'installation à volonté)
- De jouer un peu avec (vous savez, cette impression d'être comme en prod, mais sans le risque de tout pêter)
- De pouvoir déployer mes applications habituelles sur le cluster
- De pouvoir jouer un workflow complet de "développement" à partir de mes projets:
  - je déploie une 1ère fois
  - je fais des modifications, et les déploie en production
  - je fais des modifications et je les déploie dans un environnement "bac à sable"
  - etc ...

J'ai eu la chance de tomber sur les bonnes personnes (1) (sachantes) qui ont eu la patience de répondre à mes questions, et à toutes mes questions (c'est à dire, sans me répondre: "mais pourquoi tu veux faire ça? C'est complètement con!"). J'ai pris des notes, j'ai expérimenté (dès fois tard), pour finalement avoir des résultats. Donc, l'objectif de ce long article (en plusieurs parties) est de vous expliquer étape par étape comment arriver au même résultat que moi.

> **Attention**: mes expérimentations ne sont pas forcément faites pour aller en production (je le répète: **Ce n'est pas aux développeurs de gérer le cluster Kube**) faites donc bien valider par les experts. Ce que je vous offre c'est un terrain de jeux qui j'espère vous aidera à progresser.

> **Remarques**: 
> - Vous avez bien sûr le droit de n'être pas d'accord ou de poser des questions, vous pouvez le faire grâce aux issues par ici: [https://gitlab.com/learn-k3s/documentation/issues](https://gitlab.com/learn-k3s/documentation/issues)
> - Vous pouvez aussi contribuer [https://gitlab.com/learn-k3s/documentation/-/merge_requests](https://gitlab.com/learn-k3s/documentation/-/merge_requests)

> **(1) Remerciements** pour diverses choses de valeurs qui ont contribuées fortement à mes explorations:
> - [Louis Tournayre](https://twitter.com/_louidji)
> - [Horacio Gonzalez](https://twitter.com/lostinbrittany)
> - [Laurent Grangeau](https://twitter.com/laurentgrangeau)
> - [Julien Ponge](https://twitter.com/jponge)
> - [Thierry Chantier](https://twitter.com/titimoby)
> - [Ludovic Piot](https://twitter.com/lpiot)
