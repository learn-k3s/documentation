# Documentation (gros blog post)

## Avancement

- [00-title.fr.md](00-title.fr.md): ✅
- [01-introduction.fr.md](01-introduction.fr.md): ✅
- [02-first-cluster.fr.md](02-first-cluster.fr.md): ✅
- [03-deploy-and-redeploy-nodejs-app.fr.md](03-deploy-and-redeploy-nodejs-app.fr.md): ✅
- [04-deployment-automation.md](04-deployment-automation.md): ✅
- [05-deploy-vert-x-micro-services.fr.md](05-deploy-vert-x-micro-services.fr.md): 🚧 (1% 😉)

