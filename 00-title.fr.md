# Kit de survie Kubernetes pour les développeurs

>  Découvrir Kubernetes avec K3S avec une vision de développeur

Nous allons voir comment créer un cluster K3S (voyez ça comme un "petit" kube - mais j'y reviens plus loin) et comment l'utiliser en tant que développeur (c'est à dire, comment déployer des applications dessus pour expérimenter avec un véritable cluster).

Tout ce qui va suivre est une compilation de ce que j'ai pu comprendre, demander, expérimenter, etc... Ne prenez pas ces textes pour argent comptant: tout ce que j'ai fait fonctionne (en tous les cas sur ma machine 😉) et j'ai réussi à faire ce que je voulais.

J'ajouterais des chapitres au fur et à mesure de mon apprentissage.

Bonne Lecture.
