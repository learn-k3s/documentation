## Déployer son propre cluster Kube (avec plusieurs nodes) en local grâce à K3S

Pour ceux qui me connaissent, je suis partisan de tout faire en local quand c'est possible. L'objectif de cette partie et de vous expliquer comment créer un cluster Kube sur votre propre machine et ceci de la manière la plus simple possible. Et il faut que cela soit le plus reproductible possible (pourvoir se planter et recommencer rapidement facilite le cycle d'apprentissage). 
Je vous livre donc ma méthode (libre à vous de l'adapter, d'en choisr une autre, ...).

Pour mon cas personnel, j'ai choisi d'utiliser le projet **[K3S](https://rancher.com/docs/k3s/latest/en/)** de chez Rancher qui est une distribution "allégée" de Kubernetes, mais complètement fonctionnelle et largement suffisante pour mes besoins (décrits en introduction). L'avantage de K3S, c'est qu'il ne demande que très peu de ressources, vous pourrez donc l'utiliser même sur des machines peu puissantes (mais si vous avez 8Go de RAM c'est bien).

Je ferai "tourner" mon cluster dans une VM grâce à **[Multipass](https://multipass.run/)** de chez Canonical qui est un orchestrateur de VM Ubuntu existant pour OSX, Windows 10 et certaines distributions Linux.

> Le script shell que j'utilise n'a été testé que sur Mac, mais les étapes sont faciles à reproduire à la main pour Windows (j'accepte toute contribution d'utilisateur Windows bien sûr pour compléter cet article 😉)

## Pré-requis

Il faut installer **[Multipass](https://multipass.run/)**. Vous trouverez toutes les informations nécessaires ici: 

- Pour Linux: [https://multipass.run/docs/installing-on-linux](https://multipass.run/docs/installing-on-linux)
- Pour Mac: [https://multipass.run/docs/installing-on-macos](https://multipass.run/docs/installing-on-macos) (la bersion la plus simple étant d'utiliser `brew`)
- Pour Windows: [https://multipass.run/docs/installing-on-windows](https://multipass.run/docs/installing-on-windows)

Vous devez aussi installer la CLI Kubernetes **[KubeCtl](https://kubernetes.io/docs/reference/kubectl/overview/)** sur votre poste: [https://cloud.google.com/kubernetes-engine/docs/quickstart](https://cloud.google.com/kubernetes-engine/docs/quickstart)

## Installation du Cluster

Je me suis largement inspiré de l'excellent article [Local K3s Cluster Made Easy With Multipass](https://medium.com/better-programming/local-k3s-cluster-made-easy-with-multipass-108bf6ce577c) de [Luc Juggery](https://twitter.com/lucjuggery), donc je vous livre mon script (pour les explications détaillées, l'article de Luc est très bien)

Commencez par céer un répertoire, et dans ce répertoire, ajoutez un fichier de configuration `cluster.config` (*c'est ma façon de faire, mais vous pouvez bien sûr faire autrement*):

```text
node1_name="basestar";
node2_name="raider-one";
node3_name="raider-two";
```

Et au même endroit, créez un fichier `create.cluster.sh` (et donnez lui les droits d'exécution: `chmod +x create.cluster.sh`):

```shell
#!/bin/sh
eval $(cat cluster.config)

multipass launch -n ${node1_name} --cpus 2 --mem 2G
multipass launch -n ${node2_name} --cpus 2 --mem 2G
multipass launch -n ${node3_name} --cpus 2 --mem 2G

# Initialize K3s on node1
echo "👋 Initialize 📦 K3s on ${node1_name}..."

multipass --verbose exec ${node1_name} -- sh -c "
  curl -sfL https://get.k3s.io | sh -
"

TOKEN=$(multipass exec ${node1_name} sudo cat /var/lib/rancher/k3s/server/node-token)
IP=$(multipass info ${node1_name} | grep IPv4 | awk '{print $2}')

echo "😃 📦 K3s initialized on ${node1_name} ✅"
echo "🤫 Token: ${TOKEN}"
echo "🖥 IP: ${IP}"

# Join node2 and node3 to the Cluster
echo "👋 Join ${node2_name} and ${node3_name} to the Cluster"

# Joining node2
multipass --verbose exec ${node2_name} -- sh -c "
  curl -sfL https://get.k3s.io | K3S_URL='https://$IP:6443' K3S_TOKEN='$TOKEN' sh -
"

echo "😃 ${node2_name} has joined the Cluster  ✅"

# Joining node3
multipass --verbose exec ${node3_name} -- sh -c "
  curl -sfL https://get.k3s.io | K3S_URL='https://$IP:6443' K3S_TOKEN='$TOKEN' sh -
"

echo "😃 ${node3_name} has joined the Cluster  ✅"


multipass exec ${node1_name} sudo cat /etc/rancher/k3s/k3s.yaml > k3s.yaml

sed -i '' "s/127.0.0.1/$IP/" k3s.yaml
```

Il ne vous reste plus qu'à lancer `./create.cluster.sh`


🖐 **Attention** les 2 lignes ci-dessous du script:

```shell
multipass exec ${node1_name} sudo cat /etc/rancher/k3s/k3s.yaml > k3s.yaml

sed -i '' "s/127.0.0.1/$IP/" k3s.yaml
```
vont permettre de copier la configuration kubernetes de la VM vers votre poste ainsi que de modifier l'adresse IP avec la valeur de l'IP de la VM. Ainsi lorsque de votre poste (donc à l'extérieur de la VM) vous voudrez passer des commandes avec KubeCtl vous devrez:

- passez cette commande: `export KUBECONFIG=$PWD/k3s.yaml` (modifiez le chemin vers le fichier `yaml` si vous n'êtes pas dans le même répertoire)
- puis vous pourrez passer votre commande `kubectl` comme par exemple `kubectl top nodes`

👋 **Vous trouverez tous les scripts nécessaires ici**: https://gitlab.com/learn-k3s/create-cluster

## Le cluster "tourne"

Si tout c'est bien passé (normalement, si votre connexion internet est bonne, vous en avez pour 3 à 5 minutes), vous devriez trouver le fichier de configuration de votre cluster à la racine de votre répertoire: `k3s.yaml` (qui va permettre à `kubectl`) de se connecter au cluster.

Et si vous n'avez pas oublié d'installer `kubectl` sur votre machine hôte, si vous tapez les commandes suivantes:

```shell
export KUBECONFIG=./k3s.yaml
kubectl top nodes
```

Vous allez obtenir la liste des nodes(noeuds) de votre cluster:

```text
NAME         CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
basestar     101m         2%     702Mi           8%        
raider-one   15m          0%     267Mi           6%        
raider-two   19m          0%     265Mi           6%      
```

> Remarque: si vous obtenez le message `error: metrics not available yet` c'est que le cluster n'a pas fini de démarrer. Attendez 2,3 minutes avant de re-essayer.

Vous avez donc fait un grand pas, vous avec un (petit) cluster kube qui tourne sur votre machine


## Arrêter, Démarrer le cluster

Vous trouverez les scripts nécessaires dans ce repository: [https://gitlab.com/learn-k3s/create-cluster](https://gitlab.com/learn-k3s/create-cluster) 

## Outillage: K9S

**K3S** n'embarque pas de dashboard (même s'il est possible de l'installer), mais pour la suite nous n'en n'aurons pas besoin. A la place nous allons utiliser un outil smilaire mais en mode texte, dont vous ne pourrez plus vous passer, je veux parler de **K9S**: [https://github.com/derailed/k9s](https://github.com/derailed/k9s)

**K9S** va vous permettre de vous "promener" dans votre cluster, de voir l'état de vos déploiements, et beaucoup d'autres choses. La procédure d'installation est simple (vous trouverez tout ce qu'il fauut dans le `README` du projet).

Tapez les commandes suivantes:

```shell
export KUBECONFIG=./k3s.yaml
k9s --all-namespaces 
```

Vous obtiendrez ceci:

![alt k9s](/pictures/k9s-first-time.png)


Maintenant vous êtes prêts à déployer 🚀 des 🌍 webapps 😉



